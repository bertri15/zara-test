Zara Test by Bertran Guasch

Esta aplicación se ha desarrollado con la librería React. Para ello se ha utilizado la aplicación create-react-app para la creación del proyecto. 

_Entorno de Desarrollo_
Para ejecutar el entorno de desarollo se debe utilizar los comandos en la ruta de la aplicación:
"npm install"
"npm start"
Para instalar las liberías (la primera vez) y para iniciar el servidor local respectivamente. (se puede acceder a la aplicación des del navegador con la ruta localhost:3000), 

_Entorno de producción_
Para compilar la aplicación para producción se peude usar el comando 
"npm run build" 
que creará la aplicación estática en la carpeta build en una versión concatenada y minimizada preparada para el servidor de producción.


_Las aplicaciones y librerías usadas son(con versión):_
"node": "6.11.4",
"npm": "5.8",
"axios": "^0.18.0",
"react": "^16.4.1",
"react-dom": "^16.4.1",
"react-redux": "^5.0.7",
"react-router-dom": "^4.3.1",
"react-scripts": "1.1.4",
"redux": "^4.0.0",
"redux-thunk": "^2.3.0",
"xml2js": "^0.4.19"