import React, { Component, Fragment } from 'react';
import './App.css';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import Error404 from './components/error-404';
import Header from './containers/header';
import List from './containers/list';
import PodcastWrapper from './containers/podcast-wrapper';

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <Fragment>
            <Route path="/" component={Header}/>
            <Switch>
              <Route path="/podcast/:podcastId" component={PodcastWrapper} />
              <Route exact path="/" component={List} />
              <Route component={Error404} />
            </Switch>
          </Fragment>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
