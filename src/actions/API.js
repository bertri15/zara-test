import axios from 'axios';

const CORS_EXTENSION = 'https://cors.io/?';
const ROOT_URL = 'https://itunes.apple.com/';


export function get(url, flags){
  return axios.get(`${flags.requireCors ? CORS_EXTENSION : ''}${flags.requireRoot ? ROOT_URL : ''}${url}`, {
  });
}