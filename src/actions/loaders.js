import { LOADING,
 } from './types';

export function loading(isLoading){
  return ({type: LOADING, payload: isLoading});
}