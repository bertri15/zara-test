import * as API from './API';
import { PODCASTS_LIST,
  PODCAST_INFO
 } from './types';
import {parseString} from 'xml2js';
import {loading} from './loaders';

function validDate(oldDate){
  //returns true (valid date) or false (more than 24h passed)
  const ts = Math.round(new Date().getTime() / 1000);
  const tsYesterday = ts - (24 * 60 * 60);
  if(oldDate>tsYesterday){
    return true
  }
  return false;
}

function getValidList(){
  //returns a list of valid podcasts (in order to erase old ones and store LESS data)
  let podcasts = JSON.parse(localStorage.getItem('podcasts'));
  if(podcasts === null){
    return {podcasts : []};
  }
  else{
    const validPodcasts = podcasts.podcasts.filter((podcast) => {
      if(validDate(podcast.date)) {
        return true;
      }
      else return false;
    });
    return {podcasts:validPodcasts};
  }
}

export function getList(){
  const current = JSON.parse(localStorage.getItem('list'));
  if (current === null || !validDate(current.date)){
    //storage version not valid
    localStorage.removeItem('list');
    const url = 'us/rss/toppodcasts/limit=100/genre=1310/json';
    return function(dispatch){
      dispatch(loading(true));
      API.get(url, {requireRoot: true, requireCors: true})
        .then(response => {
          const result = response.data.feed.entry.map( (entry) => {
            return {id: entry.id.attributes['im:id'], 
            name: entry['im:name'].label, 
            author: entry['im:artist'].label,
            img: entry['im:image'][entry['im:image'].length-1].label}
          } );
          localStorage.setItem('list', JSON.stringify({date: Math.round(new Date().getTime() / 1000),data: result}));
          dispatch({ type: PODCASTS_LIST, payload: result});
          dispatch(loading(false));
        })
        .catch(response => {
          console.log(response);
          dispatch({ type: PODCASTS_LIST, payload: []});
          dispatch(loading(false));
        });
    }
  }
  else {
    return ({ type: PODCASTS_LIST, payload: current.data});
  }
}

export function getPodcast(id){
  const podcastsCache = JSON.parse(localStorage.getItem('podcasts'));
  let current;
  if (podcastsCache === null){
    current = null;
  }
  else {
    current = podcastsCache.podcasts.find((podcast) => {
      return (podcast.data.id === id);
    });
    if(current === undefined){
      current = null;
    }
  }
  if (current === null || !validDate(current.date)){
    const url = `lookup?id=${id}`;
    return function(dispatch){
      dispatch(loading(true));
      API.get(url, {requireRoot: true, requireCors: true})
        .then(response => {
          API.get(response.data.results[0].feedUrl, {requireCors: true})
          .then(response => {
            parseString(response.data, function (err, result) {
              const podcast = {
                id,
                name : result.rss.channel['0'].title[0],
                author : result.rss.channel['0']['itunes:author'][0],
                description : result.rss.channel['0'].description[0],
                img : result.rss.channel['0']['itunes:image'][0].$.href,
                episodes : result.rss.channel['0'].item.map((episode, index) => {
                  return {
                    id : index, //episodes tend to have different identifiers depending on the podcast, array index seems like a better option since the list will be on the local store
                    name : episode.title[0], 
                    date : episode.pubDate[0],
                    duration : episode['itunes:duration']!==undefined ? episode['itunes:duration'][0] : null, //some durations appeared to be missing from the xml in some cases
                    description : episode['description']!==undefined ? episode['description'][0] : null,
                    url : episode.enclosure[0].$.url
                  }
                })
              };
              let newPodcasts = getValidList();
              newPodcasts.podcasts.push({date: Math.round(new Date().getTime() / 1000), data: podcast});
              localStorage.removeItem('podcasts');
              localStorage.setItem('podcasts', JSON.stringify(newPodcasts));
              dispatch({ type: PODCAST_INFO, payload: podcast});
              dispatch(loading(false));
            });
          })
          .catch(response => {
            console.log(response);
            dispatch({ type: PODCAST_INFO, payload: {}});
            dispatch(loading(false));
          });
        })
        .catch(response => {
          console.log(response);
          dispatch({ type: PODCAST_INFO, payload: {}});
          dispatch(loading(false));
        });
    };
  }
  else {
    return ({ type: PODCAST_INFO, payload: current.data});
  }
}



export function emptyPodcast(){
  return ({ type: PODCAST_INFO, payload: null});
}