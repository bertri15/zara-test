import React, { Component } from 'react';

class Error404 extends Component {
  render() {
    return (
      <div>
        <h2>Error 404: The page you're trying to access does not exist!</h2>
      </div>
    );
  }
}

export default Error404;
