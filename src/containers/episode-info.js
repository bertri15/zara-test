import React, { Component } from 'react';
import { connect } from 'react-redux';

class EpisodeInfo extends Component {
  render() {
    let render = "";
    if(this.props.podcastsList.podcast_info !== null){
      const episode = this.props.podcastsList.podcast_info.episodes[this.props.match.params.episodeId];
      render = (
        <div>
          <div>
            {episode.name}
          </div>
          <div dangerouslySetInnerHTML={{__html: episode.description}}>

          </div>
          <div>
            <audio controls>
              <source src={episode.url} type="audio/mpeg" />
              Your browser does not support the audio element.
            </audio>
          </div>
        </div>
      );
    }
    return (
      <div className="episodeInfo shadowBox">
        {render}
      </div>
    );
  }
}

function mapStateToProps({podcastsList}){
  return {podcastsList};
}


export default connect (mapStateToProps)(EpisodeInfo);

