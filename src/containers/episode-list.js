import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

class EpisodeList extends Component {
  render() {
    let render = "";
    if(this.props.podcastsList.podcast_info !== null){
      render = (
        <Fragment>
          <div className="episodesCounter shadowBox">
            Episodes: {this.props.podcastsList.podcast_info.episodes.length}
          </div>
          <div className="shadowBox">
            <div>
              <b>
                <div className="episode">
                  <div style={{color:"black"}}>Name</div> <div>Date</div><div>Duration</div> 
                </div>
              </b>
              <div className="episodeList">
                {this.props.podcastsList.podcast_info.episodes.map((episode) => {
                  const date = new Date(Date.parse(episode.date));
                  const day = `${date.getDay()}/${date.getMonth()}/${date.getFullYear()}`;
                  return (
                    <Link key={episode.id} to={`/podcast/${this.props.podcastsList.podcast_info.id}/episode/${episode.id}`}>
                      <div className="episode">
                        <div>{episode.name}</div> <div>{day}</div><div>{episode.duration}</div> 
                      </div>
                    </Link>
                  );
                })}
              </div>
            </div>
          </div>
        </Fragment>
        
        
      );
    }
    return (
      <div className="podcastInfo">
        {render}
      </div>
    );
  }
}

function mapStateToProps({podcastsList}){
  return {podcastsList};
}


export default connect (mapStateToProps)(EpisodeList);

