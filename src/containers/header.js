import React, { Component } from 'react';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom';
import Loading from '../media/images/loading.gif';

class Header extends Component {
  render() {
    return (
      <div className="headerSpacer">
        <div className="header">
          <Link to="/" className="headerText">Podcaster</Link>
          {this.props.loading.loading ? <img className="headerLoading" src={Loading} alt="Loading"/> : ""}
        </div>
      </div>
    );
  }
}

function mapStateToProps({loading}){
  return {loading};
}


export default connect (mapStateToProps)(Header);