import React, { Component } from 'react';
import {connect} from 'react-redux';
import {getList} from '../actions/podcasts-list';
import {loading} from '../actions/loaders';
import {Link} from 'react-router-dom';

class List extends Component {
  constructor(props) {
    super(props);
    this.state = {filter: ""};
    this.handleFilterText = this.handleFilterText.bind(this);
  }
  componentDidMount(){
    this.props.getList();
  }
  handleFilterText(event){
    this.setState({filter: event.target.value});
  }
  filterMatches(){
    if(this.props.podcastsList.podcasts_list !== null){
      const matches = this.props.podcastsList.podcasts_list.filter((podcast) => {
        return (podcast.author.toLowerCase().includes(this.state.filter.toLowerCase())) || (podcast.name.toLowerCase().includes(this.state.filter.toLowerCase()));
      });
      return matches;
    }
    else{
      return [];
    }
  }
  renderList(matches){
    let render = null;
    render = matches.map( (podcast) => {
      return (
      <Link key={podcast.id} to={`/podcast/${podcast.id}`}>
        <div className="listPodcastElement shadowBox">
          <div><img src={podcast.img} alt=""/></div>
          <div>
            <p className="listPodcastElementTitle">{podcast.name}</p>
            <p className="listPodcastElementAuthor">Author: {podcast.author}</p>
          </div>
        </div>
      </Link>
      );
    });
    return render;
  }
  render() {
    const filteredList = this.filterMatches();
    return (
      <div className="container">
        <div className="searchBar">
          <span>{filteredList.length}</span>
          <input type="text" placeholder="Filter podcasts..." value={this.state.filter} onChange={this.handleFilterText}/>
        </div>
        <div className="list">
          {this.renderList(filteredList)}
        </div>
      </div>
    );
  }
}

function mapStateToProps({podcastsList}){
  return {podcastsList};
}


export default connect (mapStateToProps,{getList, loading})(List);
