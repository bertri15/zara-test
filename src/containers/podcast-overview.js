import React, { Component } from 'react';
import { connect } from 'react-redux';

class PodcastOverview extends Component {
  render() {
    let render = "";
    if(this.props.podcastsList.podcast_info !== null){
      render = (
        <div>
          <img src={this.props.podcastsList.podcast_info.img} alt=""/>
          <p><b>{this.props.podcastsList.podcast_info.name}</b></p>
          <p><i>by {this.props.podcastsList.podcast_info.author}</i></p>
          <p>Description:</p>
          <p><i dangerouslySetInnerHTML={{__html: this.props.podcastsList.podcast_info.description}}></i></p>
        </div>
      );
    }
    return (
      <div className="podcastOverview shadowBox">
        {render}
      </div>
    );
  }
}

function mapStateToProps({podcastsList}){
  return {podcastsList};
}


export default connect (mapStateToProps)(PodcastOverview);

