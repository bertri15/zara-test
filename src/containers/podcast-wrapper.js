import React, { Component } from 'react';
import PodcastOverview from '../containers/podcast-overview';
import { Switch, Route } from 'react-router-dom';
import { getPodcast, emptyPodcast } from '../actions/podcasts-list';
import { loading } from '../actions/loaders';
import { connect } from 'react-redux';
import EpisodeList from './episode-list';
import EpisodeInfo from './episode-info';

class podcastWrapper extends Component {
  componentDidMount(){
    this.props.emptyPodcast();
    this.props.getPodcast(this.props.match.params.podcastId);
  }
  render() {
    return (
      <div className="podcastWrapper">
        <div><PodcastOverview /></div>
        <Switch>
          <Route exact path="/podcast/:podcastId/" component={EpisodeList} />
          <Route exact path="/podcast/:podcastId/episode/:episodeId" component={EpisodeInfo} />
        </Switch>
      </div>
    );
  }
}

function mapStateToProps({podcastsList}){
  return {podcastsList};
}


export default connect (mapStateToProps,{getPodcast,loading, emptyPodcast})(podcastWrapper);
