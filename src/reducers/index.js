import {combineReducers} from 'redux';
import podcastListReducer from './podcasts-list';
import loadersReducer from './loaders';

const rootReducer = combineReducers({
  podcastsList: podcastListReducer,
  loading: loadersReducer,
});

export default rootReducer;