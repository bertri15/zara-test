import {
  LOADING
} from '../actions/types';

export default function (state = {loading: false}, action){
  switch (action.type){
    case LOADING:
      return {...state, loading: action.payload};
    default:
      return state;
  }
}