import {
  PODCASTS_LIST,
  PODCAST_INFO
} from '../actions/types';

export default function (state = {podcasts_list : null, podcast_info: null}, action){
  switch (action.type){
    case PODCASTS_LIST:
      return {...state, podcasts_list: action.payload};
    case PODCAST_INFO:
      return {...state, podcast_info: action.payload};
    default:
      return state;
  }
}